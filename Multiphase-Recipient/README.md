# Multiphase Recipient

run with ```interFoam > log.solver &```

create a file with .foam extension to read data in ParaView ```touch multiphase_recipient.foam```

![animation](https://gitlab.com/HeNeos/openfoam-codes/-/raw/master/Multiphase-Recipient/pictures/multiphase_recipient.gif)
